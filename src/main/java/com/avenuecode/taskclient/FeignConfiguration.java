package com.avenuecode.taskclient;

import feign.Logger;
import feign.Logger.Level;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

  @Bean
  public Logger.Level logLevel() {
    return Level.FULL;
  }
}
