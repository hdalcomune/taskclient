package com.avenuecode.taskclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "taskapp", url = "http://localhost:8080", path = "/taskapp/")
public interface TaskClient {
  @RequestMapping(method = RequestMethod.GET, value = "/task/{id}")
  TaskResponse getTask(@PathVariable("id") Long id);

  @RequestMapping(method = RequestMethod.POST, path = "/task")
  TaskResponse createTask(@RequestBody TaskResponse taskResponse);
}
