package com.avenuecode.taskclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class TaskClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskClientApplication.class, args);
	}
}
