package com.avenuecode.taskclient;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {

  private final TaskClient taskClient;

  public TaskController(TaskClient taskClient) {
    this.taskClient = taskClient;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/taskapp/task/{id}")
  public TaskResponse getTask(@PathVariable("id") long id) {
    return taskClient.getTask(id);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/taskapp/task")
  public TaskResponse createTask(@RequestBody TaskResponse taskResponse) {
    return taskClient.createTask(taskResponse);
  }
}
