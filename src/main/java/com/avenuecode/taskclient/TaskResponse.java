package com.avenuecode.taskclient;

import java.util.Objects;
import lombok.Data;

@Data
public class TaskResponse {

  public Long id;
  public String name;
  public String description;
  public String status;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TaskResponse that = (TaskResponse) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(description, that.description) &&
        Objects.equals(status, that.status);
  }
  @Override
  public int hashCode() {

    return Objects.hash(name, description, status);
  }
}
